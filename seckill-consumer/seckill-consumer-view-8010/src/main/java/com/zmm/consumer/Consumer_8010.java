package com.zmm.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

/**
 * @author zmm
 * @date 2021/3/23 15:44
 */
@SpringBootApplication(exclude = SecurityAutoConfiguration.class)
public class Consumer_8010 {
    public static void main(String[] args) {
        SpringApplication.run(Consumer_8010.class, args);
    }
}
