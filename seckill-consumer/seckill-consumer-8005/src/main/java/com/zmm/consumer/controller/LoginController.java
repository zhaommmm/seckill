package com.zmm.consumer.controller;

import com.zmm.api.dto.UserDto;
import com.zmm.api.result.Result;
import com.zmm.api.service.IUserClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author zmm
 * @date 2021/3/22 16:42
 */
@RestController
@RequestMapping("/login")
public class LoginController {

    @Autowired
    private IUserClientService userClientService;

    @GetMapping("/findAll")
    public Result<List<UserDto>> findAllUser(){
        return this.userClientService.userFindAll();
    }

    @PostMapping("/find")
    public Result<UserDto> findUserById(@RequestBody UserDto userDto){
        return this.userClientService.userFindById(userDto);
    }

    @PostMapping("/add")
    Result<String> addUser(@RequestBody UserDto userDto){
        System.out.println(userDto.toString());
        return this.userClientService.userAdd(userDto);
    }

    @PostMapping("/delete")
    Result<String> deleteUser(@RequestBody UserDto userDto){
        return this.userClientService.userDelete(userDto);
    }


}
