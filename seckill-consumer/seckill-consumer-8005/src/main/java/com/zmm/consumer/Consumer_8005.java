package com.zmm.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author zmm
 * @date 2021/3/22 16:12
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableEurekaClient
@EnableCircuitBreaker
@EnableFeignClients(basePackages = "com.zmm.api.service")
//@ComponentScan和@SpringBootApplication一起使用的时候，@SpringBootApplication中的扫描注解将失效，所以在@Component注解中需要将本项目需要扫描和引入的其他项目需要扫描的包都要写进去才行。
@ComponentScan({"com.zmm.api.fallback","com.zmm.consumer","com.zmm.rabbitmq","com.zmm.redis"})
//在微服务启动的时候就能去加载自定义的rule类
@RibbonClient(name = "PROVIDER")
public class Consumer_8005 {
    public static void main(String[] args) {
        SpringApplication.run(Consumer_8005.class,args);
    }
}
