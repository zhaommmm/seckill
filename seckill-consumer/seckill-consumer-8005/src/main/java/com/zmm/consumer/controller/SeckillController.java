package com.zmm.consumer.controller;

import com.alibaba.fastjson.JSONObject;
import com.zmm.api.dto.GoodDto;
import com.zmm.api.dto.SeckillDto;
import com.zmm.api.result.Result;
import com.zmm.api.service.ISeckillClientService;
import com.zmm.api.utils.ObjectTransformUtil;
import com.zmm.rabbitmq.server.RabbitService;
import com.zmm.redis.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * @author zmm
 * @date 2021/3/23 11:20
 */
@RestController
@RequestMapping("/seckill")
public class SeckillController {

    @Autowired
    private ISeckillClientService seckillClientService;

    @Autowired
    private RabbitService rabbitService;

    @Autowired
    private RedisUtil redisUtil;

    @PostMapping("/good/seckillGood")
    Result<String> seckillGood(@RequestBody SeckillDto seckillDto){

        Object o = redisUtil.getCacheByKey(seckillDto.getGoodId().toString());
        if (o == null){
            return Result.failed("请选择正确的抢购商品ID");
        }
        GoodDto good = JSONObject.toJavaObject(JSONObject.parseObject(String.valueOf(o)), GoodDto.class);
        Date date = new Date();
        if (date.before(good.getStartTime())){
            return Result.failed("抢购还未开始");
        }
        if (date.after(good.getEndTime())){
            return Result.failed("抢购已结束");
        }

        if (redisUtil.getCacheByKey(""+seckillDto.getUserId()) != null){
            return Result.success("恭喜你，抢购成功！");
        }else{
            rabbitService.send(seckillDto);
            return Result.waited("请稍等......");
        }
    }

    @PostMapping("/good/get")
    Result<GoodDto> getGoodById(@RequestBody GoodDto goodDto) {
        return this.seckillClientService.getGoodById(goodDto);
    }

    @GetMapping("/good/getAll")
    List<Result<GoodDto>> getAllGood(){
        return this.seckillClientService.getAllGood();
    }

    @PostMapping("/good/update")
    Result<String> updateGood(@RequestBody GoodDto goodDto){
        return this.seckillClientService.updateGood(goodDto);
    }

    @PostMapping("/good/add")
    Result<String> addGood(@RequestBody GoodDto goodDto){
        return this.seckillClientService.addGood(goodDto);
    }

    @PostMapping("/good/del")
    Result<String> delGood(@RequestBody GoodDto goodDto){
        return this.seckillClientService.delGood(goodDto);
    }
}
