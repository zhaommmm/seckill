CREATE TABLE `good` (
  `good_id` bigint NOT NULL COMMENT '商品id',
  `good_name` varchar(150) NOT NULL COMMENT '商品名',
  `good_img` varchar(250) NOT NULL COMMENT '商品图片',
  `good_price` float NOT NULL DEFAULT '0' COMMENT '商品价格',
  `cost_price` float NOT NULL DEFAULT '0' COMMENT '折扣价',
  `start_time` datetime NOT NULL COMMENT '开始时间',
  `end_time` datetime NOT NULL COMMENT '结束时间',
  `surplus_count` bigint NOT NULL DEFAULT '0' COMMENT '剩余商品数',
  `total_count` bigint NOT NULL DEFAULT '0' COMMENT '商品总数',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '修改时间',
  `version` int NOT NULL DEFAULT '1' COMMENT '版本号',
  `is_deleted` int NOT NULL DEFAULT '0' COMMENT '是否删除，0表示未删除，1表示已删除。',
  PRIMARY KEY (`good_id`),
  UNIQUE KEY `good_id_UNIQUE` (`good_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='商品信息表';

CREATE TABLE `orders` (
  `order_id` bigint NOT NULL COMMENT '订单主键',
  `total_money` float NOT NULL DEFAULT '0' COMMENT '总价格',
  `cost_money` float NOT NULL DEFAULT '0' COMMENT '折扣后的价格',
  `user_id` bigint NOT NULL COMMENT '用户id',
  `good_id` bigint NOT NULL COMMENT '商品id',
  `order_status` int NOT NULL DEFAULT '0' COMMENT '订单支付状态，0表示未支付，1表示已支付。',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '修改时间',
  `version` int NOT NULL DEFAULT '1' COMMENT '版本号',
  `is_deleted` int NOT NULL DEFAULT '0' COMMENT '是否删除，0表示未删除，1表示已删除。',
  PRIMARY KEY (`order_id`),
  UNIQUE KEY `order_id_UNIQUE` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='订单信息表';

CREATE TABLE `user` (
  `user_id` bigint NOT NULL COMMENT '用户主键',
  `user_name` varchar(60) NOT NULL COMMENT '用户名',
  `password` varchar(60) NOT NULL COMMENT '用户密码',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '修改时间',
  `version` int NOT NULL DEFAULT '1' COMMENT '版本号',
  `is_deleted` int NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`),
  UNIQUE KEY `user_name_UNIQUE` (`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户信息表'