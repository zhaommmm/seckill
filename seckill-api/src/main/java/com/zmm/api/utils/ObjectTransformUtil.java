package com.zmm.api.utils;

import org.springframework.beans.BeanWrapperImpl;

import org.springframework.beans.BeanUtils;

import java.beans.PropertyDescriptor;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author zmm
 * @date 2021/3/23 9:50
 */
public class ObjectTransformUtil extends BeanUtils{

    /**
     * 把对象数据复制到目标对象
     * @param source 源数据
     * @param target 目标对象实例
     */
    public static void copyProperties(Object source, Object target){
        copyProperties(source, target, getNullPropertyNames(source));
    }

    /**
     * 根据指定类复制对象并返回结果
     * @param source 源数据
     * @param clazz 目标对象class类
     * @param <T> 目标对象类型
     * @return 返回目标对象实例
     */
    public static <T> T copyProperties(Object source, Class<T> clazz){
        T target;
        try {
            target = clazz.newInstance();
        }catch (Exception e){
            String msg = MessageFormat.format("对象无法实例化", clazz);
            throw new RuntimeException(msg);
        }
        copyProperties(source, target, getNullPropertyNames(source));
        return target;
    }

    /**
     * 获取bean对象中值为空的属性名称
     * @param source 源数据对象
     * @return 值为空的属性名称数组
     */
    public static String[] getNullPropertyNames(Object source){
        BeanWrapperImpl src = new BeanWrapperImpl(source);
        PropertyDescriptor[] pds = src.getPropertyDescriptors();
        Set<String> emptyNames = new HashSet<>();
        for (PropertyDescriptor pd:pds) {
            Object srcValue = src.getPropertyValue(pd.getName());
            if (srcValue == null){
                emptyNames.add(pd.getName());
            }
        }
        int size = emptyNames.size();
        return emptyNames.toArray(new String[size]);
    }

    public  static  <T> List<T> toList(T... nodeA)
    {
        return Arrays.asList(nodeA);
    }

    public  static  <T> List<T> asList(T... nodeA)
    {
        return Arrays.asList(nodeA);
    }
    public  static  <T>  T[] toArray(T... array)
    {
        return array;
    }
}
