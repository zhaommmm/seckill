package com.zmm.api.service;

import com.zmm.api.dto.GoodDto;
import com.zmm.api.dto.SeckillDto;
import com.zmm.api.fallback.SeckillClientFallback;
import com.zmm.api.result.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author zmm
 * @date 2021/3/22 16:38
 */
//value的值是spring.application.name的名字，可以在provider的yml文件中看，也可以在eureka界面的application下看
@FeignClient(value = "PROVIDER",
        contextId = "SeckillProvider",
        fallbackFactory = SeckillClientFallback.class)
public interface ISeckillClientService {

    @PostMapping("/good/get")
    Result<GoodDto> getGoodById(@RequestBody GoodDto goodDto);

    @GetMapping("/good/getAll")
    List<Result<GoodDto>> getAllGood();

    @PostMapping("/good/update")
    Result<String> updateGood(@RequestBody GoodDto goodDto);

    @PostMapping("/good/add")
    Result<String> addGood(@RequestBody GoodDto goodDto);

    @PostMapping("/good/del")
    Result<String> delGood(@RequestBody GoodDto goodDto);
}
