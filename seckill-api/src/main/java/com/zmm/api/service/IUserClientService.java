package com.zmm.api.service;

import com.zmm.api.dto.UserDto;
import com.zmm.api.fallback.UserClientFallback;
import com.zmm.api.result.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author zmm
 * @date 2021/3/22 16:50
 */
@FeignClient(value = "USER-PROVIDER",
             contextId = "UserProvider",
             fallbackFactory = UserClientFallback.class)
public interface IUserClientService {

    @PostMapping("/user/get")
    Result<UserDto> userFindById(@RequestBody UserDto userDto);

    @GetMapping("/user/findAll")
    Result<List<UserDto>> userFindAll();

    @PostMapping("/user/add")
    Result<String> userAdd(@RequestBody UserDto userDto);

    @PostMapping("/delete")
    Result<String> userDelete(@RequestBody UserDto userDto);
}
