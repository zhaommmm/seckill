package com.zmm.api.result;

import lombok.Data;

import java.io.Serializable;

/**
 * @author zmm
 * @date 2021/3/23 8:34
 */
@Data
public class Result<T> implements Serializable {

    private static final long serializableId = 1L;

    private static final int STATUS_SUCCESS = 1;

    private static final int STATUS_FAILED = -1;

    private static final int STATUS_WAIT = 0;

    private int status;

    private String msg;

    private T data;

    public Result(){

    }

    public Result(int status, String msg, T data) {
        this.status = status;
        this.msg = msg;
        this.data = data;
    }

    /**
     * 请求成功
     */
    public static <T> Result<T> success(T data){

        return new Result<>(STATUS_SUCCESS, "请求成功", data);
    }

    public static <T> Result<T> success(String msg, T data){
        return new Result<>(STATUS_SUCCESS, msg, data);
    }

    public static <T> Result<T> success(String msg){
        return new Result<>(STATUS_SUCCESS, msg, null);
    }

    public static <T> Result<T> success(){
        return new Result<>(STATUS_SUCCESS, "请求成功", null);
    }

    /**
     * 请求失败
     */
    public static <T> Result<T> failed(){
        return new Result<>(STATUS_FAILED, "请求失败", null);
    }

    public static <T> Result<T> failed(String msg){
        return new Result<>(STATUS_FAILED, msg, null);
    }

    public static <T> Result<T> failed(String msg, T data){
        return new Result<>(STATUS_FAILED, msg, data);
    }

    public static <T> Result<T> failed(int status){
        return new Result<>(status, "请求失败", null);
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }



    /**
     * 请求等待
     */
    public static <T> Result<T> waited(){
        return new Result<>(STATUS_WAIT, "请等待。", null);
    }

    public static <T> Result<T> waited(String msg){
        return new Result<>(STATUS_WAIT, msg, null);
    }



    @Override
    public String toString() {
        return "Result{" +
                "status=" + status +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }

    public boolean isSuccess(){
        return status == STATUS_SUCCESS;
    }
}
