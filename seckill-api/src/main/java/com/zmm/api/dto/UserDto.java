package com.zmm.api.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author zmm
 * @date 2021/3/22 19:36
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserDto implements Serializable {
    private final static long serializableId = 1L;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;
    private String userName;
    private String password;
}
