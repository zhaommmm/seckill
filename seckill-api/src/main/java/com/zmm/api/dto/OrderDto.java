package com.zmm.api.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zmm
 * @date 2021/3/23 8:16
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderDto implements Serializable {

    private static final long serializableId = 1L;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long orderId;

    private Float costMoney;
    private Float totalMoney;

    private Date createTime;

    private Date updateTime;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long goodId;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;

    private Integer orderStatus;
}
