package com.zmm.api.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zmm
 * @date 2021/3/23 8:11
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GoodDto implements Serializable {
    private static final long serializable = 1L;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long goodId;
    private String goodName;
    private String goodImg;
    private Float goodPrice;
    private Float costPrice;
    private Date startTime;
    private Date endTime;
    private Long surplusCount;
    private boolean isSeckill = false;


}
