package com.zmm.api.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author zmm
 * @date 2021/3/23 8:08
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SeckillDto implements Serializable {
    private static final long serializableId = 1L;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long goodId;

    //库存
    private Long newSurplusCount;
}
