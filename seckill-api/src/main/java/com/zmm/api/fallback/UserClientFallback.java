package com.zmm.api.fallback;

import com.zmm.api.dto.UserDto;
import com.zmm.api.result.Result;
import com.zmm.api.service.IUserClientService;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author zmm
 * @date 2021/3/22 19:33
 */
@Component
@Slf4j
public class UserClientFallback implements FallbackFactory<IUserClientService> {

    @Override
    public IUserClientService create(Throwable cause) {
        return new IUserClientService() {
            @Override
            public Result<UserDto> userFindById(UserDto userDto) {
                log.debug("[DE BUG] [userFindById] ===>" + cause);
                return Result.failed("没有找到该用户信息。"+cause);
            }

            @Override
            public Result<List<UserDto>> userFindAll() {
                log.debug("[DE BUG] [userFindAll] ===>" + cause);
                return Result.failed("没有找到用户信息。"+cause);
            }

            @Override
            public Result<String> userAdd(UserDto userDto) {
                log.debug("[DE BUG] [userAdd] ===>" + cause);
                return Result.failed("添加用户信息失败。"+cause);
            }

            @Override
            public Result<String> userDelete(UserDto userDto) {
                log.debug("[DE BUG] [userDelete] ===>" + cause);
                return Result.failed("删除用户信息失败。"+cause);
            }
        };
    }
}
