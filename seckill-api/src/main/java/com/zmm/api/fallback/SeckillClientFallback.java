package com.zmm.api.fallback;

import com.zmm.api.dto.GoodDto;
import com.zmm.api.dto.SeckillDto;
import com.zmm.api.result.Result;
import com.zmm.api.service.ISeckillClientService;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zmm
 * @date 2021/3/20 14:53
 */
@Component
@Slf4j
public class SeckillClientFallback implements FallbackFactory<ISeckillClientService> {
    /**
     * 创建UserClient客户端的回退处理实例
     * @return
     */
    @Override
    public ISeckillClientService create(Throwable cause) {
        /**
         * 创建一个UserClient客户端接口的匿名回退实例
         */
        return new ISeckillClientService(){

            @Override
            public Result<GoodDto> getGoodById(GoodDto goodDto) {
                log.debug("[DE BUG] [getGoodById] ===>" + cause);
                GoodDto good = GoodDto.builder().goodId(0L)
                                                .goodName("null")
                                                .goodImg("")
                                                .goodPrice(0f)
                                                .costPrice(0f)
                                                .build();
                return Result.failed("未查询到商品信息！", good);
            }

            @Override
            public List<Result<GoodDto>> getAllGood() {
                log.debug("[DE BUG] [getAllGood] ===>" + cause);
                GoodDto good = GoodDto.builder().goodId(0L)
                                                .goodName("null")
                                                .goodImg("")
                                                .goodPrice(0f)
                                                .costPrice(0f)
                                                .build();
                List<Result<GoodDto>> results = new ArrayList<>();
                results.add(Result.failed("未查询到商品信息！", good));
                return  results;
            }

            @Override
            public Result<String> updateGood(GoodDto goodDto) {
                log.debug("[DE BUG] [updateGood] ===>" + cause);
                return Result.failed("修改商品信息失败！"+cause);
            }

            @Override
            public Result<String> addGood(GoodDto goodDto) {
                log.debug("[DE BUG] [addGood] ===>" + cause);
                return Result.failed("添加商品失败！"+cause);
            }

            @Override
            public Result<String> delGood(GoodDto goodDto) {
                log.debug("[DE BUG] [delGood] ===>" + cause);
                return Result.failed("删除商品失败！"+cause);
            }

        };
    }
}
