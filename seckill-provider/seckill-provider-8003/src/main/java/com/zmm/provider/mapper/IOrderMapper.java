package com.zmm.provider.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zmm.provider.enity.OrderEnity;
import org.springframework.stereotype.Repository;

/**
 * @author zmm
 * @date 2021/3/23 9:20
 */
@Repository
public interface IOrderMapper extends BaseMapper<OrderEnity> {
}
