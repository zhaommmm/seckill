package com.zmm.provider.controller;

import com.zmm.api.dto.OrderDto;
import com.zmm.api.result.Result;
import com.zmm.provider.service.impl.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 订单管理控制层
 *
 * @author zmm
 * @date 2021/3/23 7:47
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping("/get")
    public Result<OrderDto> getOrderById(@RequestBody OrderDto orderDto){
        return Result.success(orderService.findOrderById(orderDto));
    }

    @GetMapping("/getAll")
    public List<Result<OrderDto>> getAllOrder(){
        List<Result<OrderDto>> results = new ArrayList<>();
        List<OrderDto> allOrder = orderService.findAllOrder();
        for (OrderDto orderDto : allOrder) {
            results.add(Result.success(orderDto));
        }
        return results;
    }

    @PostMapping("/update")
    public Result<String> updateOrder(@RequestBody OrderDto orderDto){
        Result<String> result;
        if (orderService.modifyOrder(orderDto)){
            result = Result.success("修改成功！");
        }else{
            result = Result.failed("修改失败！");
        }
        return result;
    }

    @PostMapping("/add")
    public Result<String> addOrder(@RequestBody OrderDto orderDto){
        Result<String> result;
        if (orderService.addOrder(orderDto)){
            result = Result.success("添加成功！");
        }else{
            result = Result.failed("添加失败！");
        }
        return result;
    }

    @PostMapping("/del")
    public Result<String> delOrder(@RequestBody OrderDto orderDto){
        Result<String> result;
        if (orderService.deleteOrder(orderDto)){
            result = Result.success("删除成功！");
        }else{
            result = Result.failed("删除失败！");
        }
        return result;
    }
}
