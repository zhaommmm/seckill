package com.zmm.provider.service;

import com.zmm.api.dto.GoodDto;
import com.zmm.api.dto.SeckillDto;

import java.util.List;

/**
 * @author zmm
 * @date 2021/3/23 9:19
 */
public interface IGoodService {

    boolean seckillGood(SeckillDto seckillDto);

    //下面是测试方法==================================================

    boolean addGood(GoodDto goodDto);

    boolean modifyGood(GoodDto goodDto);

    boolean deleteGood(GoodDto goodDto);

    List<GoodDto> findAllGood();

    GoodDto findGoodById(GoodDto goodDto);
}
