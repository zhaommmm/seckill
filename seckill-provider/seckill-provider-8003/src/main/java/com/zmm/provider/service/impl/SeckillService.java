package com.zmm.provider.service.impl;

import com.zmm.api.dto.SeckillDto;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author zmm
 * @date 2021/3/31 20:47
 */
@Service
public class SeckillService {

    @Autowired
    private GoodService goodService;

    @RabbitListener(queues = {"seckill.direct.queue"})
    public void reviceSeckillMsg(SeckillDto seckillDto){
         goodService.seckillGood(seckillDto);
    }
}
