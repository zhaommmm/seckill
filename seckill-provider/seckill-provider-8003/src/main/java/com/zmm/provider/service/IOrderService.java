package com.zmm.provider.service;

import com.zmm.api.dto.OrderDto;
import com.zmm.api.dto.SeckillDto;

import java.util.List;

/**
 * @author zmm
 * @date 2021/3/23 9:20
 */
public interface IOrderService {

    OrderDto findOrderByUserIdAndGoodId(SeckillDto seckillDto);

    boolean addOrder(OrderDto orderDto);

    boolean modifyOrder(OrderDto orderDto);

    boolean deleteOrder(OrderDto orderDto);

    List<OrderDto> findAllOrder();

    OrderDto findOrderById(OrderDto orderDto);
}
