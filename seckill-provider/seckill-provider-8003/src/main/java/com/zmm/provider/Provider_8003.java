package com.zmm.provider;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author zmm
 * @date 2021/3/22 16:34
 */
@SpringBootApplication(exclude = SecurityAutoConfiguration.class)
@EnableDiscoveryClient
@MapperScan("com.zmm.provider.mapper")
@ComponentScan({"com.zmm.redis","com.zmm.provider","com.zmm.rabbitmq"})
public class Provider_8003 {
    public static void main(String[] args) {
        SpringApplication.run(Provider_8003.class,args);
    }
}