package com.zmm.provider.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.zmm.api.dto.GoodDto;
import com.zmm.api.dto.OrderDto;
import com.zmm.api.dto.SeckillDto;
import com.zmm.api.utils.ObjectTransformUtil;
import com.zmm.provider.enity.GoodEnity;
import com.zmm.provider.mapper.IGoodMapper;
import com.zmm.provider.service.IGoodService;
import com.zmm.redis.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.redisson.Redisson;
import org.redisson.api.RLock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zmm
 * @date 2021/3/23 9:19
 */
@Service
@Slf4j
public class GoodService implements IGoodService {

    @Autowired
    private IGoodMapper goodMapper;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private Redisson redisson;

    @Autowired
    private OrderService orderService;


    @Override
    public boolean seckillGood(SeckillDto seckillDto) {
        log.info("seckillGood已调用");

        boolean flag = false;
        Object o = redisUtil.getCacheByKey(seckillDto.getGoodId().toString());
        GoodDto goodDto = JSONObject.toJavaObject(JSONObject.parseObject(String.valueOf(o)), GoodDto.class);

        if (goodDto == null){
            throw new RuntimeException("没有该秒杀商品");
        }

        //获取库存
        Long surplusCount = goodDto.getSurplusCount();
        //判断是否阻塞（双重锁判断）
        if (surplusCount > 0){
            RLock lock = redisson.getLock("seckillGood");
            //加锁
            lock.lock();
            try {
                o = redisUtil.getCacheByKey(seckillDto.getGoodId().toString());
                goodDto =  JSONObject.toJavaObject(JSONObject.parseObject(String.valueOf(o)), GoodDto.class);
                assert goodDto != null;
                //判断是否有其他的线程
                if (goodDto.getSurplusCount()>0){
                    //执行下单 、 减库存事务
                    seckillTransaction(seckillDto, goodDto);
                }
                flag = true;
            }catch (Exception e){
                e.printStackTrace();
            }finally {
                lock.unlock();
            }
        }
        return flag;
    }

    /**
     * 下订单、减库存事务
     */
    @Transactional
    public void seckillTransaction(SeckillDto seckillDto, GoodDto goodDto){
        log.info("seckillTransaction已调用");

        //判断订单是否重复
        if (orderService.findOrderByUserIdAndGoodId(seckillDto) != null){
            log.debug("订单已存在，请勿重复创建订单。");
            throw new RuntimeException("订单已存在，请勿重复创建订单。");
        }

        //创建订单
        OrderDto orderDto = OrderDto.builder()
                                .goodId(seckillDto.getGoodId())
                                .userId(seckillDto.getUserId())
                                .costMoney(goodDto.getCostPrice())
                                .totalMoney(goodDto.getGoodPrice())
                                .orderStatus(0)
                                .build();

        //添加订单
        if (!orderService.addOrder(orderDto)){
            log.info("添加订单进数据库失败。");
            throw new RuntimeException("添加订单进数据库失败。");
        }
        //OrderDto order = orderService.findOrderByUserIdAndGoodId(seckillDto);
        if (!redisUtil.setCacheValueTime(""+seckillDto.getUserId(), orderDto, 180)){
            log.info("添加订单进缓存失败。");
            throw new RuntimeException("添加订单进缓存失败。");
        }


        //减库存
        goodDto.setSurplusCount(goodDto.getSurplusCount() - 1);
        if(!modifyGood(goodDto)){
            log.info("修改数据库库存失败。");
            throw new RuntimeException("修改数据库库存失败。");
        }
        if(!redisUtil.setCacheValueTime(goodDto.getGoodId().toString(), goodDto, 180)){
            log.info("修改缓存库存失败。");
            throw new RuntimeException("修改缓存库存失败。");
        }

        System.out.println(goodDto.getSurplusCount());
    }


    //下面是测试方法==================================================

    @Override
    public boolean addGood(GoodDto goodDto) {
        log.info("addGood已调用");
        redisUtil.setCacheValueTime(goodDto.getGoodId().toString(), goodDto, 180);
        GoodEnity goodEnity = ObjectTransformUtil.copyProperties(goodDto, GoodEnity.class);
        goodEnity.setTotalCount(goodEnity.getSurplusCount());
        return goodMapper.insert(goodEnity) > 0;
    }

    @Override
    public boolean modifyGood(GoodDto goodDto) {
        log.info("modifyGood已调用");
        if (!redisUtil.setCacheValueTime(goodDto.getGoodId().toString(), goodDto, 180)){
            throw new RuntimeException("修改缓存失败...");
        }
        return goodMapper.updateById(ObjectTransformUtil.copyProperties(goodDto, GoodEnity.class)) > 0;
    }

    @Override
    public boolean deleteGood(GoodDto goodDto) {
        log.info("deleteGood已调用");
        redisUtil.deleteCacheByKey(goodDto.getGoodId().toString());
        return goodMapper.deleteById(goodDto.getGoodId()) > 0;
    }

    @Override
    public List<GoodDto> findAllGood() {
        log.info("findAllGood已调用");
        List<Object> allCache = redisUtil.getAllCache();
        List<GoodDto> goodDtos = new ArrayList<>();
        if (allCache.size() != 0){
            for (Object cache:allCache){
                goodDtos.add((GoodDto) cache);
            }
        }else{
            List<GoodEnity> goodEnities = goodMapper.selectBatchIds(null);
            for (GoodEnity goodEnity : goodEnities) {
                GoodDto goodDto = ObjectTransformUtil.copyProperties(goodEnity, GoodDto.class);
                goodDtos.add(goodDto);
                redisUtil.setCacheValueTime(goodDto.getGoodId().toString(), goodDto, 300);
            }
        }
        return goodDtos;
    }

    @Override
    public GoodDto findGoodById(GoodDto goodDto) {
        log.info("findGoodById已调用");
        Object o = redisUtil.getCacheByKey(goodDto.getGoodId().toString());
        GoodDto dto = JSONObject.toJavaObject(JSONObject.parseObject(String.valueOf(o)) , GoodDto.class);
        if (dto == null){
            GoodEnity goodEnity = goodMapper.selectById(goodDto.getGoodId());
            if (goodEnity == null){
                throw new RuntimeException("不存在该商品");
            }
            dto = ObjectTransformUtil.copyProperties(goodEnity, GoodDto.class);
            redisUtil.setCacheValueTime(dto.getGoodId().toString(), dto, 300);
        }
        return dto;
    }
}
