package com.zmm.provider.enity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zmm
 * @date 2021/3/22 17:13
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "good")
@Builder
public class GoodEnity implements Serializable {

    @TableId
    @JsonSerialize(using = ToStringSerializer.class)
    private Long goodId;

    @TableField(value = "good_name")
    private String goodName;

    @TableField(value = "good_img")
    private String goodImg;

    @TableField(value = "good_price")
    private Float goodPrice;

    @TableField(value = "cost_price")
    private Float costPrice;

    //活动开始时间
    @TableField(value = "start_time")
    private Date startTime;

    //活动结束时间
    @TableField(value = "end_time")
    private Date endTime;

    //剩余商品数
    @TableField(value = "surplus_count")
    private Long surplusCount;

    //总商品数
    @TableField(value = "total_count", fill = FieldFill.INSERT)
    private Long totalCount;

    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    @Version
    @TableField(value = "version")
    private Integer version;

    @TableLogic
    @TableField(value = "is_deleted")
    private Integer isDeleted;
}
