package com.zmm.provider.service.impl;

import com.zmm.api.dto.OrderDto;
import com.zmm.api.dto.SeckillDto;
import com.zmm.api.utils.ObjectTransformUtil;
import com.zmm.provider.enity.OrderEnity;
import com.zmm.provider.mapper.IOrderMapper;
import com.zmm.provider.service.IOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zmm
 * @date 2021/3/23 9:20
 */
@Service
@Slf4j
public class OrderService implements IOrderService {

    @Autowired
    private IOrderMapper orderMapper;

    /**
     * 通过用户id和商品id查询订单是否存在
     * @param seckillDto            秒杀Dto信息
     * @return                      订单信息
     */
    @Override
    public OrderDto findOrderByUserIdAndGoodId(SeckillDto seckillDto) {
        log.info("findOrderByUserIdAndGoodId已调用");
        Map<String, Object> map = new HashMap<>();
        map.put("good_id", seckillDto.getGoodId());
        map.put("user_id", seckillDto.getUserId());
        List<OrderEnity> orderEnities = orderMapper.selectByMap(map);
        if (orderEnities.size() != 0){
            return ObjectTransformUtil.copyProperties(orderEnities.get(0), OrderDto.class);
        }
        return null;
    }

    @Override
    public boolean addOrder(OrderDto orderDto) {
        log.info("addOrder已调用");
        return orderMapper.insert(ObjectTransformUtil.copyProperties(orderDto, OrderEnity.class)) > 0;
    }

    @Override
    public boolean modifyOrder(OrderDto orderDto) {
        log.info("modifyOrder已调用");
        return orderMapper.updateById(ObjectTransformUtil.copyProperties(orderDto,OrderEnity.class)) > 0;
    }

    @Override
    public boolean deleteOrder(OrderDto orderDto) {
        log.info("deleteGood已调用");
        return orderMapper.deleteById(orderDto.getGoodId()) > 0;
    }

    @Override
    public List<OrderDto> findAllOrder() {
        log.info("findAllOrder已调用");
        List<OrderEnity> orderEnities = orderMapper.selectBatchIds(null);
        List<OrderDto> orderDtos = new ArrayList<>();
        for (OrderEnity orderEnity : orderEnities) {
            orderDtos.add(ObjectTransformUtil.copyProperties(orderEnity, OrderDto.class));
        }
        return orderDtos;
    }

    @Override
    public OrderDto findOrderById(OrderDto orderDto) {
        log.info("findOrderById已调用");
        return ObjectTransformUtil.copyProperties(orderMapper.selectById(orderDto.getGoodId()),OrderDto.class);
    }
}
