package com.zmm.provider.controller;

import com.zmm.api.dto.GoodDto;
import com.zmm.api.result.Result;
import com.zmm.provider.service.impl.GoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 商品管理控制层
 *
 * @author zmm
 * @date 2021/3/23 7:46
 */
@RestController
@RequestMapping("/good")
public class GoodController {

    @Autowired
    private GoodService goodService;

    @PostMapping("/get")
    public Result<GoodDto> getGoodById(@RequestBody GoodDto goodDto){
        return Result.success(goodService.findGoodById(goodDto));
    }

    @GetMapping("/getAll")
    public Result<List<GoodDto>> getAllGood(){
        List<GoodDto> allGood = goodService.findAllGood();
        if (allGood.size() == 0){
            throw new RuntimeException("未查询到用户信息");
        }
        return Result.success("查询成功！",allGood);
    }

    @PostMapping("/update")
    public Result<String> updateGood(@RequestBody GoodDto goodDto){
        Result<String> result;
        if (goodService.modifyGood(goodDto)){
            result = Result.success("修改成功！");
        }else{
            result = Result.failed("修改失败！");
        }
        return result;
    }

    @PostMapping("/add")
    public Result<String> addGood(@RequestBody GoodDto goodDto){
        Result<String> result;
        if (goodService.addGood(goodDto)){
            result = Result.success("添加成功！");
        }else{
            result = Result.failed("添加失败！");
        }
        return result;
    }

    @PostMapping("/del")
    public Result<String> delGood(@RequestBody GoodDto goodDto){
        Result<String> result;
        if (goodService.deleteGood(goodDto)){
            result = Result.success("删除成功！");
        }else{
            result = Result.failed("删除失败！");
        }
        return result;
    }
}
