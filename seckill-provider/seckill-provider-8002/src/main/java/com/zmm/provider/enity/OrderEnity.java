package com.zmm.provider.enity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zmm
 * @date 2021/3/22 17:14
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName(value = "orders")
public class OrderEnity implements Serializable {

    private final static long serializableId = 1L;

    @TableId
    @JsonSerialize(using = ToStringSerializer.class)
    private Long orderId;

    @TableField(value = "total_money")
    private Float totalMoney;

    @TableField(value = "cost_money")
    private Float costMoney;

    @TableField(value = "user_id")
    private Long userId;

    @TableField(value = "good_id")
    private Long goodId;

    @TableField(value = "order_status")
    private Integer orderStatus;

    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    @Version
    @TableField(value = "version")
    private Integer version;

    @TableLogic
    @TableField(value = "is_deleted")
    private Integer isDeleted;
}
