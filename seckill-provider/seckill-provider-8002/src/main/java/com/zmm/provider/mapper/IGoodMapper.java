package com.zmm.provider.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zmm.provider.enity.GoodEnity;
import org.springframework.stereotype.Repository;

/**
 * @author zmm
 * @date 2021/3/23 9:20
 */
@Repository
public interface IGoodMapper extends BaseMapper<GoodEnity> {
}
