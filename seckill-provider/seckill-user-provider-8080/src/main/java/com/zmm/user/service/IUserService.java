package com.zmm.user.service;


import com.zmm.api.dto.UserDto;

import java.util.List;

/**
 * @author zmm
 * @date 2021/3/22 18:47
 */
public interface IUserService {

    boolean addUser(UserDto userDto);

    boolean deleteUser(UserDto userDto);

    List<UserDto> findAllUser();

    UserDto findUserById(UserDto userDto);
}
