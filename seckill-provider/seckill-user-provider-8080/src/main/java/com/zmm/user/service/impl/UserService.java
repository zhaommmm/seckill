package com.zmm.user.service.impl;

import com.zmm.api.dto.UserDto;
import com.zmm.api.utils.ObjectTransformUtil;
import com.zmm.user.enity.UserEnity;
import com.zmm.user.mapper.IUserMapper;
import com.zmm.user.service.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zmm
 * @date 2021/3/22 18:48
 */
@Service
@Slf4j
public class UserService implements IUserService {

    @Autowired
    private IUserMapper userMapper;


    @Override
    public boolean addUser(UserDto userDto) {
        log.info("addUser已调用...");
        return userMapper.insert(ObjectTransformUtil.copyProperties(userDto,UserEnity.class)) > 0;
    }

    @Override
    public boolean deleteUser(UserDto userDto) {
        log.info("deleteUser已调用...");
        return userMapper.deleteById(userDto.getUserId()) > 0;
    }

    @Override
    public List<UserDto> findAllUser() {
        log.info("findAllUser已调用...");
        List<UserEnity> users = userMapper.selectBatchIds(null);
        List<UserDto> list = new ArrayList<>();
        for (UserEnity user : users) {
            list.add(ObjectTransformUtil.copyProperties(user, UserDto.class));
        }
        return list;
    }

    @Override
    public UserDto findUserById(UserDto userDto) {
        log.info("findUserById已调用...");
        return ObjectTransformUtil.copyProperties(userMapper.selectById(userDto.getUserId()), UserDto.class);
    }
}
