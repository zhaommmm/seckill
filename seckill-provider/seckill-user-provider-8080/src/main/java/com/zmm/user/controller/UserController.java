package com.zmm.user.controller;

import com.zmm.api.dto.UserDto;
import com.zmm.api.result.Result;
import com.zmm.user.service.impl.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author zmm
 * @date 2021/3/22 19:14
 */
@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/find")
    public Result<UserDto> userFindById(@RequestBody UserDto userDto){
        log.info("userFindById已调用");
        return Result.success(userService.findUserById(userDto));
    }

    @GetMapping("/findAll")
    public Result<List<UserDto>> userFindAll(){
        log.info("userFindAll已调用");
        return Result.success(userService.findAllUser());
    }

    @PostMapping("/add")
    public Result<String> userAdd(@RequestBody UserDto userDto){
        log.info("userAdd已调用");
        Result<String> result;
        System.out.println(userDto.toString());
        if(userService.addUser(userDto)){
            result = Result.success("添加成功");
        }else{
            result = Result.success("添加失败");
        }
        return result;
    }

    @PostMapping("/delete")
    public Result<String> userDelete(@RequestBody UserDto userDto){
        log.info("userDelete已调用");
        Result<String> result;
        if(userService.deleteUser(userDto)){
            result = Result.success("删除成功");
        }else{
            result = Result.success("删除失败");
        }
        return result;
    }
}
