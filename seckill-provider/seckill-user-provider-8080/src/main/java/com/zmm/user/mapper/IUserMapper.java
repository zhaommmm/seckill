package com.zmm.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zmm.user.enity.UserEnity;
import org.springframework.stereotype.Repository;

/**
 * @author zmm
 * @date 2021/3/22 18:31
 */
@Repository
public interface IUserMapper extends BaseMapper<UserEnity> {
}
