package com.zmm.user;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author zmm
 * @date 2021/3/22 18:14
 */
@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("com.zmm.user.mapper")
@ComponentScan({"com.zmm.redis","com.zmm.user"})
public class UserProvider_8080 {
    public static void main(String[] args) {
        SpringApplication.run(UserProvider_8080.class,args);
    }
}
