package com.zmm.rabbitmq.server;

import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

/**
 * @author zmm
 * @date 2021/3/31 20:09
 */
@Component
public class ConfirmCallbackService implements RabbitTemplate.ConfirmCallback {


    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        if (ack){
            System.out.println("抢购成功！");
        }else{
            System.out.println("抢购失败！");
        }
    }
}
