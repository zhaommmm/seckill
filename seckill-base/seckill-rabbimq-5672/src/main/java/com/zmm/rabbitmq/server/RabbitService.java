package com.zmm.rabbitmq.server;

import com.zmm.api.dto.SeckillDto;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author zmm
 * @date 2021/3/30 19:52
 */
@Component
public class RabbitService {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void send(SeckillDto seckillDto){
        rabbitTemplate.convertAndSend("direct_exchange","seckillGood", seckillDto);
    }

}
