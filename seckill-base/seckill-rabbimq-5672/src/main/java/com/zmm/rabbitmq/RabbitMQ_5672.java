package com.zmm.rabbitmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zmm
 * @date 2021/3/28 18:58
 */
@SpringBootApplication
public class RabbitMQ_5672 {
    public static void main(String[] args) {
        SpringApplication.run(RabbitMQ_5672.class, args);
    }
}
