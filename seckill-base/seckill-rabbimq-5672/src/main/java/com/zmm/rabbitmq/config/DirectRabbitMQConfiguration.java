package com.zmm.rabbitmq.config;


import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.RabbitListenerContainerFactory;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zmm
 * @date 2021/3/30 19:40
 */
@Configuration
public class DirectRabbitMQConfiguration {

    //1.声明交换机
    @Bean
    public DirectExchange directExchange(){
        return new DirectExchange("direct_exchange", true, false);
    }

    //2.声明队列
    @Bean
    public Queue seckillQueue(){
        //设置过期时间
        Map<String, Object> args = new HashMap<>();
        args.put("x-message-ttl", 300000);        //过期时间5min
        args.put("x-max-length", 20);          //设置最大队列长度
        args.put("x-dead-letter-exchange", "dead_direct_exchange"); //与死信队列绑定
        args.put("x-dead-letter-routing-key", "dead");  //fanout不需要配置
        args.put("x-overflow", "reject-publish");

        return new Queue("seckill.direct.queue", true, false, false, args);
    }



    //3.绑定队列和交换机
    @Bean
    public Binding seckillBinding(){
        return BindingBuilder.bind(seckillQueue()).to(directExchange()).with("seckillGood");
    }

    @Bean
    public MessageConverter messageConverter(){
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public RabbitListenerContainerFactory<?> rabbitListenerContainerFactory(ConnectionFactory connectionFactory){
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory);
        factory.setMessageConverter(new Jackson2JsonMessageConverter());
        return factory;
    }
}
