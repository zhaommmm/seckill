package com.zmm.redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zmm
 * @date 2021/3/24 10:40
 */
@SpringBootApplication
public class Redis_6379 {
    public static void main(String[] args) {
        SpringApplication.run(Redis_6379.class,args);
    }
}
