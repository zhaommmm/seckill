package com.zmm.redis.utils;

import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Redis工具类，自动JSON格式转换
 *
 * @author zmm
 * @date 2021/3/24 10:52
 */
@Component
public class RedisUtil {

    @Autowired
    private JedisPool jedisPool;

    /**
     * 设置缓存（String类型）
     * @param key           缓存关键字
     * @param value         缓存对象
     * @return              是否缓存成功
     */
    public boolean setKeyWithValue(String key, Object value){
        Jedis jedis = null;
        boolean flag = false;
        try{
            jedis = jedisPool.getResource();
            jedis.set(key, JSONObject.toJSONString(value));
            flag = true;
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if (jedis!=null) jedis.close();
        }
        return flag;
    }

    /**
     * 添加缓存和过期时间
     * @param key           缓存关键字
     * @param value         缓存对象
     * @param second        过期时间（s）
     * @return              是否缓存成功
     */
    public boolean setCacheValueTime(String key, Object value, int second){
        Jedis jedis = null;
        boolean flag = false;
        try{
            jedis = jedisPool.getResource();
            jedis.setex(key, second, JSONObject.toJSONString(value));
            flag = true;
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if (jedis!=null) jedis.close();
        }
        return flag;
    }

    /**
     * 通过key得到缓存
     * @param key           key
     * @return              Object类型的value
     */
    public Object getCacheByKey(String key){
        Jedis jedis = null;
        Object o = null;
        try{
            jedis = jedisPool.getResource();
            o = jedis.get(key);
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if (jedis != null) jedis.close();
        }
        return o;
    }

    /**
     * 通过key删除缓存
     * @param key           key
     * @return              是否删除成功
     */
    public boolean deleteCacheByKey(String key){
        Jedis jedis = null;
        boolean flag = false;
        try{
            jedis = jedisPool.getResource();
            jedis.del(key);
            flag = true;
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if (jedis != null) jedis.close();
        }
        return flag;
    }

    /**
     * 查询所有缓存（测试）
     * @return              所有缓存（List<Object>）
     */
    public List<Object> getAllCache(){
        Jedis jedis = null;
        List<Object> objects = new ArrayList<>();
        try {
            jedis = jedisPool.getResource();
            Set<String> keys = jedis.keys("*");
            for(String key:keys){
                objects.add(jedis.get(key));
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if (jedis != null) jedis.close();
        }
        return objects;
    }

    /**
     * 清除所有缓存
     * @return              是否清除成功
     */
    public boolean clearAllCache(){
        Jedis jedis = null;
        boolean flag = false;
        try{
            jedis = jedisPool.getResource();
            Set<String> keys = jedis.keys("*");
            for(String key:keys){
                jedis.del(key);
            }
            flag = true;
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if (jedis != null)  jedis.close();
        }
        return flag;
    }
}
