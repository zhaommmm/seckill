package com.zmm.redis.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * @author zmm
 * @date 2021/3/24 20:48
 */
@Configuration
public class JedisConnectionConfiguration {
    @Bean
    public JedisPool redisPoolFactory(){

        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxIdle(80);
        config.setMinIdle(0);
        config.setMaxTotal(100);
        config.setMaxWaitMillis(5000);

        return new JedisPool(config, "localhost", 6379, 2000, "zmm123",0);
    }
}
