package com.zmm.redis.config;

import org.redisson.Redisson;
import org.redisson.config.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author zmm
 * @date 2021/3/25 12:19
 */
@Configuration
public class RedissonConfig {
    @Bean
    public Redisson redisson(){
        Config config = new Config();
        config.useSingleServer().setAddress("redis://localhost:6379");
        config.useSingleServer().setPassword("zmm123");
        config.setLockWatchdogTimeout(10*1000);
        return (Redisson)Redisson.create(config);
    }
}