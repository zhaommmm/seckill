package com.zmm.zuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * @author zmm
 * @date 2021/3/22 16:25
 */
@SpringBootApplication
@EnableZuulProxy
@EnableDiscoveryClient
public class Zuul_9100 {
    public static void main(String[] args) {
        SpringApplication.run(Zuul_9100.class,args);
    }
}
