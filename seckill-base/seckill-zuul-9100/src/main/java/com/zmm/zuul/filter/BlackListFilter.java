//package com.zmm.zuul.filter;
//
//import com.netflix.zuul.ZuulFilter;
//import com.netflix.zuul.context.RequestContext;
//import com.netflix.zuul.exception.ZuulException;
//import org.springframework.stereotype.Component;
//
//import javax.servlet.http.HttpServletRequest;
//import java.io.IOException;
//import java.util.Arrays;
//import java.util.List;
//
//import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.FORWARD_TO_KEY;
//
///**
// * @author zmm
// * @date 2021/3/22 16:27
// */
//@Component
//public class BlackListFilter extends ZuulFilter {
//
//    /**
//     * 黑名单，正常从数据库拿
//     */
//    static List<String> blackList = Arrays.asList("a","b","aaa");
//
//
//    /**
//     * 返回自定义过滤器的类型
//     *
//     * error：异常过滤
//     * post：后置过滤
//     * pre：前置过滤
//     * route：路由过滤
//     *
//     * @return      自定义过滤器的类型
//     */
//    @Override
//    public String filterType() {
//        return "pre";
//    }
//
//    /**
//     * 返回过滤器顺序，值越小优先级越高
//     * @return          过滤器的优先级
//     */
//    @Override
//    public int filterOrder() {
//        return 0;
//    }
//
//    /**
//     * 过滤器是否生生效
//     * @return          true生效/false无效
//     */
//    @Override
//    public boolean shouldFilter() {
//        RequestContext ctx = RequestContext.getCurrentContext();
//        System.out.println("shouldFilter方法..........");
//        //判断是否被其他过滤器拦截
//        if (ctx.containsKey(FORWARD_TO_KEY)){
//            return false;
//        }
//        //获取请求
//        HttpServletRequest request = ctx.getRequest();
//        return request.getRequestURI().startsWith("/p");
//    }
//
//    /**
//     * 过滤器核心逻辑，可以进行当前请求拦截和参数定制，也可以进行后续的路由定制，同时可以进行路由
//     * 返回结果的定制等待
//     * @return
//     * @throws ZuulException
//     */
//    @Override
//    public Object run() throws ZuulException {
//        System.out.println("run方法..........");
//        RequestContext ctx = RequestContext.getCurrentContext();
//        HttpServletRequest request = ctx.getRequest();
//
//        StringBuffer requestURL = request.getRequestURL();
//        int length = requestURL.length();
//        int index = requestURL.lastIndexOf("/") + 1;
//        if (index + 1 > length){
//            return null;
//        }
//        String s = requestURL.substring(index, length);
//
//        if (!s.equals("") && blackList.contains(s)){
//            System.out.println("此用户："+s+"在黑名单中。");
//            //表示后续不需要在处理
//            ctx.setSendZuulResponse(false);
//            try {
//                ctx.getResponse().setContentType("text/html;charset=utf-8");
//                ctx.getResponse().getWriter().write("对不起，您已进入黑名单。");
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//        return null;
//    }
//}
