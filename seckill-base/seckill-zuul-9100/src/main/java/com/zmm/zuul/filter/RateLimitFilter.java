package com.zmm.zuul.filter;

import com.google.common.util.concurrent.RateLimiter;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.apache.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.FORWARD_TO_KEY;

/**
 * @author zmm
 * @date 2021/3/26 17:05
 */
@Component
public class RateLimitFilter extends ZuulFilter {

    private static final RateLimiter RATE_LIMITER = RateLimiter.create(25);

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        RequestContext ctx = RequestContext.getCurrentContext();
        if (ctx.containsKey(FORWARD_TO_KEY)){
            return false;
        }
        HttpServletRequest request = ctx.getRequest();
        return request.getRequestURI().startsWith("/p/seckill/good/seckillGood");
    }

    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        if(!RATE_LIMITER.tryAcquire()){
            //停止访问
            ctx.setSendZuulResponse(false);
            ctx.setResponseStatusCode(HttpStatus.SC_FORBIDDEN);
        }
        return null;
    }
}
