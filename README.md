# 秒杀

#### 介绍
商品秒杀高并发练习



#### 系统架构

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210401205730432.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3ptbV9fMTM3NzQ0NTI5Mg==,size_16,color_FFFFFF,t_70)



#### 技术栈

前端：一点点点点vue通过axios异步请求

后端：Springboot、Mybatis-Plus、Redis、RabbitMQ、SpringCloud（Eureka、Feign、Hystrix、Ribbon、Zuul）、Nginx



#### 环境要求

- MySQL 6.0+



#### 使用

1. 通过sql.sql添加表信息
2. 修改seckill-provider中各个项目application.yml的数据库连接账号密码
3. 自行添加一些测试数据
4. 相继启动Eureka、Zuul、provider、consumer、consumer-view
5. 进入localhost:8010//seckill.html（注意界面中axios传输的值写死了）即可



#### 测试数据











